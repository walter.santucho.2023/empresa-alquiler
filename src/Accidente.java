class Accidente {
    private int identificador;
    private String descripcion;

    public Accidente(int identificador, String descripcion) {
        this.identificador = identificador;
        this.descripcion = descripcion;
    }

    // Getter y Setter para los atributos

    public int getIdentificador() {
        return identificador;
    }

    public String getDescripcion() {
        return descripcion;
    }
}