class Auto extends Vehiculo {
    private int capacidadPasajeros;
    private int velocidadMaxima;

    public Auto(String marca, String patente, int anoFabricacion, int capacidadPasajeros, int velocidadMaxima) {
        super(marca, patente, anoFabricacion);
        this.capacidadPasajeros = capacidadPasajeros;
        this.velocidadMaxima = velocidadMaxima;
    }

    @Override
    public double calcularGastoCada100Km() {
        return 20 + (capacidadPasajeros * 10);
    }

    @Override
    public double calcularVelocidadMaxima() {
        return velocidadMaxima;
    }

    @Override
    public int getCantidadPasajeros() {
        return capacidadPasajeros;
    }
}