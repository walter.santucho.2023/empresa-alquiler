class Bicicleta extends Vehiculo {
    private int rodado;

    public Bicicleta(String marca, String patente, int anoFabricacion, int rodado) {
        super(marca, patente, anoFabricacion);
        this.rodado = rodado;
    }

    @Override
    public double calcularGastoCada100Km() {
        return 1.0;
    }

    @Override
    public double calcularVelocidadMaxima() {
        return rodado * 1.2;
    }

    @Override
    public int getCantidadPasajeros() {
        return 1;
    }
}