import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Empresa {
    private List<Vehiculo> vehiculos;

    public Empresa() {
        this.vehiculos = new ArrayList<Vehiculo>();
    }

    public void agregarVehiculo(Vehiculo unVehiculo) {
        vehiculos.add(unVehiculo);
    }

    public int maximaCantidadAccidentesRegistrados()
    {
        int maximoActual;
        maximoActual = vehiculos.get(0).cantidadDeAccidentes();

        for (Vehiculo unVehiculo : vehiculos)
        {
            if (unVehiculo.cantidadDeAccidentes() > maximoActual)
            {
                maximoActual = unVehiculo.cantidadDeAccidentes();
            }
        }

        return maximoActual;
    }

    public double maximoKilometrajeRegistrado()
    {
        double maximoActual;
        maximoActual = vehiculos.get(0).getKilometraje();

        for (Vehiculo unVehiculo: vehiculos)
        {
            if (unVehiculo.getKilometraje() > maximoActual)
            {
                maximoActual = unVehiculo.getKilometraje();
            }
        }

        return maximoActual;
    }

    public List<Vehiculo> vehiculosMasAccidentados(int maximoAccidentes)
    {
        List<Vehiculo> resultado = new ArrayList<Vehiculo>();

        for (Vehiculo unVehiculo: vehiculos)
        {
            if (unVehiculo.cantidadDeAccidentes() == maximoAccidentes)
            {
                resultado.add(unVehiculo);
            }
        }

        return resultado;
    }

    public List<Vehiculo> vehiculosMasKilometraje(int maximoKilometraje)
    {
        List<Vehiculo> resultado = new ArrayList<Vehiculo>();

        for (Vehiculo unVehiculo: vehiculos)
        {
            if (unVehiculo.getKilometraje() == maximoKilometraje)
            {
                resultado.add(unVehiculo);
            }
        }

        return resultado;
    }

    public List<Vehiculo> vehiculosMasAccidentadosYMasKilometraje(List<Vehiculo> vehiculosMasAccidentes, List<Vehiculo> vehiculosMasKm)
    {
        // Se deben intersectar las 2 listas
        Set<Vehiculo> conjuntoA = new HashSet<>(vehiculosMasAccidentes);
		Set<Vehiculo> conjuntoB = new HashSet<>(vehiculosMasKm);
		Set<Vehiculo> interseccion = new HashSet<>(conjuntoA);
		interseccion.retainAll(conjuntoB);
        List<Vehiculo> resultado = new ArrayList<>(interseccion);
        return resultado;
    }

    public List<Vehiculo> vehiculosConConsumoCada100KmMenorA(double importe)
    {
        List<Vehiculo> resultado = new ArrayList<>();

        for ( Vehiculo unVehiculo : vehiculos)
        {
            if ( unVehiculo.calcularGastoCada100Km() < importe)
            {
                resultado.add(unVehiculo);
            }
        }

        return resultado;
    }

    public Vehiculo vehiculoMayorCoeficienteEficiencia()
    {
        Vehiculo vehiculoMayorCoeficiente = vehiculos.get(0);

        for (Vehiculo unVehiculo : vehiculos)
        {
            if (unVehiculo.calcularCoeficienteEficiencia() >
                    vehiculoMayorCoeficiente.calcularCoeficienteEficiencia())
            {
                vehiculoMayorCoeficiente = unVehiculo;
            }
        }

        return vehiculoMayorCoeficiente;
    }

    public int totalPasajerosTodosVehiculos(double velocidad)
    {
        int totalPasajeros = 0;

        for (Vehiculo unVehiculo : vehiculos)
        {
            if (unVehiculo.calcularVelocidadMaxima() > velocidad)
            {
                totalPasajeros+= unVehiculo.getCantidadPasajeros();
            }
        }

		return totalPasajeros;
    }

    public List<Vehiculo> vehiculosConMasDe10Accidentes()
    {
        List<Vehiculo> resultado = new ArrayList<>();
        for ( Vehiculo unVehiculo : vehiculos)
        {
            if (unVehiculo.cantidadDeAccidentes() > 10)
            {
                resultado.add(unVehiculo);
            }
        }
        return resultado;
    }
    public List<Vehiculo> mayorA50Kilometros()
    {
        List<Vehiculo> resultado = new ArrayList<>();
        for ( Vehiculo unVehiculo : vehiculos)
        {
            if ( unVehiculo.getKilometraje() > 50)
            {
                resultado.add(unVehiculo);
            }
        }
        return resultado;
    }
}