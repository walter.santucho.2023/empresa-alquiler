class Moto extends Vehiculo {
    private int cilindrada;
    private boolean habilitadaRuta;

    public Moto(String marca, String patente, int anoFabricacion, int cilindrada, boolean habilitadaRuta) {
        super(marca, patente, anoFabricacion);
        this.cilindrada = cilindrada;
        this.habilitadaRuta = habilitadaRuta;
    }

    @Override
    public double calcularGastoCada100Km() {
        return 50 + (cilindrada / 10.0);
    }

    @Override
    public double calcularVelocidadMaxima() {
        return cilindrada / 2.0;
    }

    @Override
    public int getCantidadPasajeros() {
        if (cilindrada <= 75) {
            return 1;
        } else {
            return 2;
        }
    }
}