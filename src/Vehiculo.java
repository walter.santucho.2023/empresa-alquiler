import java.util.ArrayList;
import java.util.List;

abstract class Vehiculo {
    private String marca;
    private String patente;
    private int anoFabricacion;
    private double kilometraje;
    private List<Accidente> historialAccidentes;

    public Vehiculo(String marca, String patente, int anoFabricacion) {
        this.marca = marca;
        this.patente = patente;
        this.anoFabricacion = anoFabricacion;
        this.kilometraje = 0;
        this.historialAccidentes = new ArrayList<Accidente>();
    }

    public void agregarAccidente(Accidente unAccidente) {
        historialAccidentes.add(unAccidente);
    }

    public void actualizarKilometraje(double distancia) {
        kilometraje += distancia;
    }

    public abstract double calcularGastoCada100Km();
    public abstract double calcularVelocidadMaxima();
    public abstract int getCantidadPasajeros();

    public double calcularCoeficienteEficiencia() {
        return (getCantidadPasajeros() * calcularVelocidadMaxima()) / calcularGastoCada100Km();
    }

    public int cantidadDeAccidentes()
    {
        return historialAccidentes.size();
    }

    // Getter y Setter para los atributos
    public String getMarca() {
        return marca;
    }

    public String getPatente() {
        return patente;
    }

    public int getAnoFabricacion() {
        return anoFabricacion;
    }

    public double getKilometraje() {
        return kilometraje;
    }

    public List<Accidente> getHistorialAccidentes() {
        return historialAccidentes;
    }
}